//
//  ScanVC.swift
//  SolarHero
//
//  Created by Suttipong Jinadech on 9/9/17.
//  Copyright © 2017 Querysoft. All rights reserved.
//

import UIKit
import BarcodeScanner
import JKSteppedProgressBar

class ScanVC: UIViewController, UITextFieldDelegate, BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate{

    @IBOutlet weak var scanTitleLb: UILabel!
    @IBOutlet weak var scanBtn: UIButton!
    @IBOutlet weak var inputOptionLb: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scanHintLb: UILabel!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var inputIdBtn: UIButton!
    @IBOutlet weak var stepperProgressView: SteppedProgressBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.decorateView()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
        Decorate View
    */
    func decorateView () {
        self.scrollView.backgroundColor = ColorPalette.solarLightGay
        self.scanTitleLb.text = "Scan your electricity bill"
        self.inputOptionLb.text = "Or"
        self.scanHintLb.text = "Application will load your electricity \ncomsumption from your \nelectricity bills."
        self.inputIdBtn.setTitle("Enter ID No.", for: .normal)
        self.inputIdBtn.setTitle("Enter ID No.", for: .selected)
        self.inputIdBtn.setTitle("Enter ID No.", for: .highlighted)
        self.inputIdBtn.backgroundColor = ColorPalette.solarPurple
        self.inputIdBtn.titleLabel?.textColor = ColorPalette.solarWhite
        self.inputIdBtn.layer.cornerRadius = 4.0
        self.inputIdBtn.layer.masksToBounds = true
        self.skipBtn.setTitle("Skip", for: .normal)
        self.skipBtn.setTitle("Skip", for: .selected)
        self.skipBtn.setTitle("Skip", for: .highlighted)
        self.skipBtn.backgroundColor = ColorPalette.solarWhite
        self.skipBtn.titleLabel?.textColor = ColorPalette.solarBlack
        self.skipBtn.layer.cornerRadius = 4.0
        self.skipBtn.layer.masksToBounds = true
        self.stepperProgressView.titles = ["", "", ""]
        self.stepperProgressView.currentTab = 1
    }
    
    
    /*
        Button Actions
    */
    @IBAction func scanTapped(_ sender: Any) {
        let controller = BarcodeScannerController()
        controller.codeDelegate = self
        controller.errorDelegate = self
        controller.dismissalDelegate = self
        
        present(controller, animated: true, completion: nil)
    }
    
//    @IBAction func calculateTapped(_ sender: Any) {
//        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = appDelegate.tabBarController
//    }
    
    @IBAction func imputIDTapped(_ sender: Any) {
        let idPopupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IdPopupVC") as! IdPopupVC
        idPopupVC.modalPresentationStyle = .overCurrentContext
        self.present(idPopupVC, animated: true, completion: nil)
    }
    
    @IBAction func skipTapped(_ sender: Any) {
        self.goToResult()
    }
    
    /*
        Barcode Delegate
    */
    func barcodeScanner(_ controller: BarcodeScannerController, didCaptureCode code: String, type: String) {
        print(code)
        print(type)
        
        let delayTime = DispatchTime.now() + Double(Int64(4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            controller.dismiss(animated: true) {
                self.goToResult()
            }
        }
    }
    
    func barcodeScanner(_ controller: BarcodeScannerController, didReceiveError error: Error) {
        print(error)
    }
    
    func barcodeScannerDidDismiss(_ controller: BarcodeScannerController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func goToResult() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let resultVC = storyboard.instantiateViewController(withIdentifier: "ResultVC") as! ResultVC
        self.present(resultVC, animated: true, completion: nil)
    }
}
