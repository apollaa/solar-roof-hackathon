//
//  LoadingVC.swift
//  SolarHero
//
//  Created by Weerayoot Ngandee on 9/9/2560 BE.
//  Copyright © 2560 Querysoft. All rights reserved.
//

import UIKit

class LoadingVC: UIViewController {

    var isFromAreaScreen: Bool!
    @IBOutlet weak var loadingImageView: UIImageView!
    
    @IBAction func backTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (self.isFromAreaScreen) {
                self.goToTab()
            }else {
                self.goToMap()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Your code with delay
        if (self.isFromAreaScreen) {
            self.loadingImageView.image = UIImage(named: "loading-education-2")
        }else {
            self.loadingImageView.image = UIImage(named: "SolarHero-UIA501")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func goToMap() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mapVC = storyboard.instantiateViewController(withIdentifier: "MapVC") as! MapVC
        self.present(mapVC, animated: true, completion: nil)
    }
    
    func goToTab() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabVC = storyboard.instantiateViewController(withIdentifier: "TabVC") as! TabVC
        self.present(tabVC, animated: true, completion: nil)
    }
}
