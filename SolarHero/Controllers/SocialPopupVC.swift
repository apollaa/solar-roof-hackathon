//
//  SocialPopupVC.swift
//  SolarHero
//
//  Created by Weerayoot Ngandee on 9/10/2560 BE.
//  Copyright © 2560 Querysoft. All rights reserved.
//

import UIKit

struct PopupData {
    var category: String!
    var donationAmount: String!
    var target: String!
    var duration: String!
    var icon: String!
}

class SocialPopupVC: UIViewController {
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var donateAmountTitle: UILabel!
    @IBOutlet weak var donateAmountLb: UILabel!
    @IBOutlet weak var targetTitle: UILabel!
    @IBOutlet weak var targetLb: UILabel!
    @IBOutlet weak var durationTitle: UILabel!
    @IBOutlet weak var durationLb: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    
    var popup = [PopupData]()
    var type: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.decorateView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func decorateView() {
        
        self.popup = [PopupData(category: "EDUCATION", donationAmount: "60,000", target: "100,000", duration: "12.12.2017", icon: "ico-box1"),
                      PopupData(category: "HEALTH", donationAmount: "60,000", target: "100,000", duration: "12.12.2017", icon: "ico-box2"),
                      PopupData(category: "RELIGION", donationAmount: "60,000", target: "100,000", duration: "12.12.2017", icon: "ico-box3"),
                      PopupData(category: "ANIMAL", donationAmount: "60,000", target: "100,000", duration: "12.12.2017", icon: "ico-box4")]
        
        self.view.backgroundColor = UIColor.clear
        self.view.isOpaque = false
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.popupView.layer.cornerRadius = 10.0
        self.popupView.layer.masksToBounds = true
        self.popupView.layer.borderColor = ColorPalette.solarPurple.cgColor
        self.popupView.layer.borderWidth = 4.0
        self.shareBtn.layer.cornerRadius = 4.0
        self.shareBtn.layer.masksToBounds = true
        self.shareBtn.backgroundColor = ColorPalette.solarPurple
        
        let popData = self.popup[self.type!]
        self.titleLb.text = popData.category
        self.donateAmountLb.text = popData.donationAmount
        self.targetLb.text = popData.target
        self.durationLb.text = popData.duration
        self.iconView.image = UIImage(named: popData.icon)
        self.iconView.backgroundColor = UIColor.clear
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
