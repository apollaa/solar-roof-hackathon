//
//  TabTwoVC.swift
//  SolarHero
//
//  Created by Suttipong Jinadech on 9/9/17.
//  Copyright © 2017 Querysoft. All rights reserved.
//

import UIKit

class TabTwoVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func educationTapped(_ sender: Any) {
        let socialPopupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SocialPopupVC") as! SocialPopupVC
        socialPopupVC.modalPresentationStyle = .overCurrentContext
        socialPopupVC.type = 0
        self.present(socialPopupVC, animated: true, completion: nil)
    }
    
    @IBAction func healthTapped(_ sender: Any) {
        let socialPopupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SocialPopupVC") as! SocialPopupVC
        socialPopupVC.modalPresentationStyle = .overCurrentContext
        socialPopupVC.type = 1
        self.present(socialPopupVC, animated: true, completion: nil)
    }
    
    @IBAction func religionTapped(_ sender: Any) {
        let socialPopupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SocialPopupVC") as! SocialPopupVC
        socialPopupVC.modalPresentationStyle = .overCurrentContext
        socialPopupVC.type = 2
        self.present(socialPopupVC, animated: true, completion: nil)
    }
    
    @IBAction func animalTapped(_ sender: Any) {
        let socialPopupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SocialPopupVC") as! SocialPopupVC
        socialPopupVC.modalPresentationStyle = .overCurrentContext
        socialPopupVC.type = 3
        self.present(socialPopupVC, animated: true, completion: nil)
    }
}
