//
//  IdPopupVC.swift
//  SolarHero
//
//  Created by Weerayoot Ngandee on 9/9/2560 BE.
//  Copyright © 2560 Querysoft. All rights reserved.
//

import UIKit

class IdPopupVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var inputTextView: UITextField!
    @IBOutlet weak var hintLb: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var goBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.decorateView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func decorateView() {
        self.view.backgroundColor = UIColor.clear
        self.view.isOpaque = false
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.popupView.layer.cornerRadius = 4.0
        self.popupView.layer.masksToBounds = true
        self.inputTextView.delegate = self
        self.backBtn.backgroundColor = ColorPalette.solarLightGay
        self.backBtn.layer.cornerRadius = 4.0
        self.backBtn.layer.masksToBounds = true
        self.goBtn.backgroundColor = ColorPalette.solarPurple
        self.goBtn.layer.cornerRadius = 4.0
        self.goBtn.layer.masksToBounds = true
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let resultVC = storyboard.instantiateViewController(withIdentifier: "ResultVC") as! ResultVC
        self.present(resultVC, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.inputTextView.resignFirstResponder()
        return true
    }
}
