//
//  TabThreeVC.swift
//  SolarHero
//
//  Created by Suttipong Jinadech on 9/9/17.
//  Copyright © 2017 Querysoft. All rights reserved.
//

import UIKit

class TabThreeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func goCommunityTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let communityVC = storyboard.instantiateViewController(withIdentifier: "CommunityVC") as! CommunityVC
        self.present(communityVC, animated: true, completion: nil)
    }
}
