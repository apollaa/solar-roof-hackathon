//
//  OpeningVC.swift
//  SolarHero
//
//  Created by Suttipong Jinadech on 9/9/17.
//  Copyright © 2017 Querysoft. All rights reserved.
//

import UIKit
import OnboardingKit

class OpeningVC: UIViewController {

    @IBOutlet weak var sceneView: OnboardingView!
    @IBOutlet weak var nextBtn: UIButton!
    
    /*
        Property
    */
    private let model = SceneObj()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.decorateView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /*
        Decorate View
    */
    func decorateView() {
        self.nextBtn.alpha = 0
        
        self.sceneView.dataSource = model
        self.sceneView.delegate = model
        
        model.didShow = { page in
            if page == 2 {
                UIView.animate(withDuration: 0.3) {
                    self.nextBtn.alpha = 1
                }
            }
        }
        
        model.willShow = { page in
            if page != 2 {
                self.nextBtn.alpha = 0
            }
        }
    }

    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
        Button Actions
    */
    @IBAction func nextBtnTapped(_ sender: Any) {
        // Go to next step
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let scanVC = storyboard.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        self.present(scanVC, animated: true, completion: nil)
    }
}
