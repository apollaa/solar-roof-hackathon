//
//  MapVC.swift
//  SolarHero
//
//  Created by Weerayoot Ngandee on 9/9/2560 BE.
//  Copyright © 2560 Querysoft. All rights reserved.
//

import UIKit
import GoogleMaps
import JKSteppedProgressBar

class MapVC: UIViewController, GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var calculateBtn: UIButton!
    @IBOutlet weak var stepperProgressView: SteppedProgressBar!
    
    /*
        Property
    */
    let path = GMSMutablePath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.decoratView()
    }
    @IBAction func backTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
        Decorate View
    */
    func decoratView() {
        // 18.732267, 98.997326
        let camera = GMSCameraPosition.camera(withLatitude: 18.732267, longitude: 98.997326, zoom: 18)
        self.mapView.delegate = self
        self.mapView.mapType = .satellite
        self.mapView.camera = camera
        self.stepperProgressView.titles = ["", "", ""]
        self.stepperProgressView.currentTab = 2
        self.calculateBtn.layer.cornerRadius = 4.0
        self.calculateBtn.layer.masksToBounds = true
        self.calculateBtn.backgroundColor = ColorPalette.solarPurple
    }
    
    /*
        Polygon Functions
    */
    func drawPolygon() {
        let rectangle = GMSPolygon(path: self.path)
        rectangle.map = self.mapView
        
        print("Your rooftop area : \(GMSGeometryArea(self.path))")
    }
    
    /*
        Map Funtions
    */
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D){
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
//        self.mapView.clear() // clearing Pin before adding new
        let marker = GMSMarker(position: coordinate)
        marker.isDraggable = true
        marker.isFlat = true
        marker.map = self.mapView
        path.add(CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude))
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        
    }
    
    @IBAction func calculateTapped(_ sender: Any) {
        self.drawPolygon()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let resultAreaVC = storyboard.instantiateViewController(withIdentifier: "ResultAreaVC") as! ResultAreaVC
        resultAreaVC.roofArea = GMSGeometryArea(self.path)
        self.present(resultAreaVC, animated: true, completion: nil)
    }
}
