//
//  TabVC.swift
//  SolarHero
//
//  Created by Weerayoot Ngandee on 9/10/2560 BE.
//  Copyright © 2560 Querysoft. All rights reserved.
//

import UIKit

class TabVC: UIViewController {

    @IBOutlet weak var headerImageVIew: UIImageView!
    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTabbar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func setupTabbar() {
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        let controller1 : TabOneVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabOneVC") as! TabOneVC
        controller1.title = "MYSELF"
        controllerArray.append(controller1)
        
        let controller2 : TabTwoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabTwoVC") as! TabTwoVC
        controller2.title = "SOCIAL"
        controllerArray.append(controller2)
        
        let controller3 : TabThreeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabThreeVC") as! TabThreeVC
        controller3.title = "SOCIETY"
        controllerArray.append(controller3)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(ColorPalette.solarPurple),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(ColorPalette.solarPurple),
            .bottomMenuHairlineColor(ColorPalette.solarPurple),
            .unselectedMenuItemLabelColor(ColorPalette.solarBlack),
            .menuItemFont(UIFont(name: "Montserrat-Regular", size: 14.0)!),
            .menuHeight(40),
            .menuItemWidth(90.0),
            .centerMenuItems(true)
        ]
        
        // Initialize scroll menu
        self.pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 216.0, width: self.view.frame.width, height: self.view.frame.height - 216.0), pageMenuOptions: parameters)
        self.addChildViewController(self.pageMenu!)
        self.view.addSubview(self.pageMenu!.view)
        self.pageMenu!.didMove(toParentViewController: self)
    }
    
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    
    // MARK: - Container View Controller
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
}
