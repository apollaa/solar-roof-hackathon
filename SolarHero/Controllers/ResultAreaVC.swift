//
//  ResultAreaVC.swift
//  SolarHero
//
//  Created by Weerayoot Ngandee on 9/9/2560 BE.
//  Copyright © 2560 Querysoft. All rights reserved.
//

import UIKit

class ResultAreaVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var electricIconView: UIImageView!
    @IBOutlet weak var billPriceLb: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var goToMapBtn: UIButton!
    @IBOutlet weak var percentageLb: UILabel!
    
    @IBOutlet weak var oldBillPrice: UILabel!
    var data = [ConsumptionData]()
    var resultData = [ResultData]()
    var roofArea: Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.decorateView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func decorateView() {
        
        self.data = [ConsumptionData(month: "1", paidAmount: 943.08, kwhAmount: 257, ft: -95.84),
                     ConsumptionData(month: "2", paidAmount: 864.83, kwhAmount: 238, ft: -88.75),
                     ConsumptionData(month: "3", paidAmount: 1404.32, kwhAmount: 369, ft: -137.6),
                     ConsumptionData(month: "4", paidAmount: 1722.64, kwhAmount: 444, ft: -165.57),
                     ConsumptionData(month: "5", paidAmount: 1657.07, kwhAmount: 416, ft: -103.04),
                     ConsumptionData(month: "6", paidAmount: 1432.52, kwhAmount: 364, ft: -90.16),
                     ConsumptionData(month: "7", paidAmount: 1219.89, kwhAmount: 314, ft: -77.78)]
        
        let offset = (self.view.frame.size.width - 224.0) / 2
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:0, left:offset, bottom:0, right:offset)
        layout.itemSize = CGSize(width: 224.0, height: 260.0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = offset
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
        self.collectionView.isPagingEnabled = true
        
        self.calculateResult()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let result = self.resultData[indexPath.item] as ResultData
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCell", for: indexPath as IndexPath) as! ResultCell
        cell.contentView.layer.borderWidth = 4.0
        cell.contentView.layer.borderColor = ColorPalette.solarPurple.cgColor
        cell.iconView.image = UIImage(named: result.icon!)
        cell.typeLb.text = result.category
        cell.dataLb.text = result.value
        cell.unitLb.text = result.unit
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.resultData.count
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offset = (self.view.frame.size.width - 224.0) / 2
        let pageWidth = Float(224.0 + offset)
        let targetXContentOffset = Float(targetContentOffset.pointee.x)
        let contentWidth = Float(collectionView!.contentSize.width  )
        var newPage = Float(self.pageControl.currentPage)
        
        if velocity.x == 0 {
            newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
        } else {
            newPage = Float(velocity.x > 0 ? self.pageControl.currentPage + 1 : self.pageControl.currentPage - 1)
            if (newPage == contentWidth / pageWidth) {
                newPage = ceil(contentWidth / pageWidth) - 1.0
            }
        }
        self.pageControl.currentPage = Int(newPage)
        let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
        targetContentOffset.pointee = point
    }
    
    @IBAction func heroTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loadingVC = storyboard.instantiateViewController(withIdentifier: "LoadingVC") as! LoadingVC
        loadingVC.isFromAreaScreen = true;
        self.present(loadingVC, animated: true, completion: nil)
    }
    
    func calculateResult() {
        
        self.resultData = [ResultData(icon: "SolarHero-UI-B601", category: "SAVE ENERGY", value: "8.0", unit: "kW/hr per month."),
                           ResultData(icon: "SolarHero-UI-B602", category: "SAVE MONEY", value: "5.0", unit: "baht per year."),
                           ResultData(icon: "SolarHero-UI-B603", category: "PAYBACK PRERIOD", value: "3.0", unit: "years.")]
        
        var totalPaid: Double = 0.0
        var totalKwh: Double = 0.0
        var avgPaid: Double = 0.0
        var avgKwh: Double = 0.0
        
        for loadData in self.data {
            totalPaid += loadData.paidAmount!
            totalKwh += loadData.kwhAmount!
        }
        
        let someDoubleFormat = ".2"
        let someDoubleFormat1 = ".1"
        avgPaid = totalPaid / Double(self.data.count)
        avgKwh = totalKwh / Double(self.data.count)
        
        let unitSize = 10.0
        let Kwatt = (unitSize/6.6)
        let cost = Kwatt * 40000.0
        let perDay = Kwatt * 4
        let perMonth = perDay * 30
        let perYear = perMonth * 12
        let savePerYear = perYear * 3.5
        let payBack = cost / savePerYear
        let billSave = perMonth * 3.5
        let billPercentageSave = (perMonth/avgKwh) * 100

        
        self.resultData[0].value = "\(perMonth.format(f: someDoubleFormat1))"
        self.resultData[1].value = "\(savePerYear.format(f: someDoubleFormat1))"
        self.resultData[2].value = "\(payBack.format(f: someDoubleFormat1))"
        self.billPriceLb.text = "\(billSave.format(f: someDoubleFormat))"
        self.percentageLb.text  = "\(billPercentageSave.format(f: someDoubleFormat1))%"
        self.oldBillPrice.text = "\(avgPaid.format(f: someDoubleFormat))"
        self.collectionView.reloadData()
    }
}
