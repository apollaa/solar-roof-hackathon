//
//  SceneObj.swift
//  SolarHero
//
//  Created by Suttipong Jinadech on 9/9/17.
//  Copyright © 2017 Querysoft. All rights reserved.
//

import UIKit
import OnboardingKit

class SceneObj: NSObject , OnboardingViewDelegate, OnboardingViewDataSource {
    
    public var didShow: ((Int) -> Void)?
    public var willShow: ((Int) -> Void)?
    
    public func numberOfPages() -> Int {
        return 3
    }
    
    public func onboardingView(_ onboardingView: OnboardingView, configurationForPage page: Int) -> OnboardingConfiguration {
        switch page {
            
        case 0:
            return OnboardingConfiguration(
                image: UIImage(named: "intro-bl-A-gb")!,
                itemImage: UIImage(named: "intro-bl-A")!,
                pageTitle: "YOU MAY NOT REALIZED",
                pageDescription: "\n\n\n\"Loss\" has been occurred\nalong the way when sending\ntraditional power.",
                backgroundImage: UIImage(named: "intro-bl-A-bg"),
                topBackgroundImage: nil,
                bottomBackgroundImage: nil//UIImage(named: "wave")
            )
            
        case 1:
            return OnboardingConfiguration(
                image: UIImage(named: "intro-bl-B-gb")!,
                itemImage: UIImage(named: "intro-bl-B")!,
                pageTitle: "",
                pageDescription: "\n\n\nEstimated loss from distribution\nin 10 years. Comparing with\nlight bulbs",
                backgroundImage: UIImage(named: "intro-bl-B-bg"),
                topBackgroundImage: nil,
                bottomBackgroundImage: nil//UIImage(named: "wave")
            )
            
        case 2:
            return OnboardingConfiguration(
                image: UIImage(named: "intro-bl-C-gb")!,
                itemImage: UIImage(named: "intro-bl-C")!,
                pageTitle: "",
                pageDescription: "\n\n\nCarbon emission from LOSS in distribution system ",
                backgroundImage: UIImage(named: "intro-bl-C-bg"),
                topBackgroundImage: nil,
                bottomBackgroundImage: nil//UIImage(named: "wave")
            )
        
        default:
            fatalError("Out of range!")
        }
    }
    
    public func onboardingView(_ onboardingView: OnboardingView, configurePageView pageView: PageView, atPage page: Int) {
        pageView.titleLabel.textColor = UIColor.white
        pageView.titleLabel.layer.shadowOpacity = 0.6
        pageView.titleLabel.layer.shadowColor = UIColor.black.cgColor
        pageView.titleLabel.layer.shadowOffset = CGSize(width: 0, height: 1)
        pageView.titleLabel.layer.shouldRasterize = true
        pageView.titleLabel.layer.rasterizationScale = UIScreen.main.scale
        pageView.titleLabel.font = UIFont(name: "Montserrat-Regular", size: 14.0)
        pageView.descriptionLabel.font = UIFont(name: "Montserrat-Regular", size: 18.0)
        
        if DeviceTarget.IS_IPHONE_4 {
            pageView.titleLabel.font = UIFont(name: "Montserrat-Regular", size: 14.0)
            pageView.descriptionLabel.font = UIFont(name: "Montserrat-Regular", size: 18.0)
        }
    }
    
    public func onboardingView(_ onboardingView: OnboardingView, didSelectPage page: Int) {
        print("Did select pge \(page)")
        didShow?(page)
    }
    
    public func onboardingView(_ onboardingView: OnboardingView, willSelectPage page: Int) {
        print("Will select page \(page)")
        willShow?(page)
    }
}
