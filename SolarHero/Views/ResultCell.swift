//
//  ResultCell.swift
//  SolarHero
//
//  Created by Weerayoot Ngandee on 9/9/2560 BE.
//  Copyright © 2560 Querysoft. All rights reserved.
//

import UIKit

class ResultCell: UICollectionViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var dataLb: UILabel!
    @IBOutlet weak var typeLb: UILabel!
    @IBOutlet weak var unitLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
