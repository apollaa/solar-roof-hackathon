//
//  ColorPalette.swift
//  SolarHero
//
//  Created by Weerayoot Ngandee on 9/9/2560 BE.
//  Copyright © 2560 Querysoft. All rights reserved.
//

import UIKit

struct ColorPalette {
    static let solarPurple = UIColor(red: 73/255.0, green: 27/255.0, blue: 89/255.0, alpha: 1)
    static let solarYello = UIColor(red: 255/255.0, green: 201/255.0, blue: 65/255.0, alpha: 1)
    static let solarLightGay = UIColor(red: 219/255.0, green: 219/255.0, blue: 219/255.0, alpha: 1)
    static let solarGay = UIColor(red: 137/255.0, green: 137/255.0, blue: 137/255.0, alpha: 1)
    static let solarWhite = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    static let solarBlack = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1)
    static let solarGreen = UIColor(red: 62/255.0, green: 132/255.0, blue: 20/255.0, alpha: 1)
}
